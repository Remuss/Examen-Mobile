const path = require('path');

module.exports = {
  entry: './src/app.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public')
  },

  module: {
    rules: [{
        test: /\.scss$/,
        use: [
            "style-loader", // creates style nodes from JS strings
            "css-loader", // translates CSS into CommonJS
            "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
    },
    {
                 test: /\.(png|svg|jpg|gif)$/,
                
                 use: [
                   'file-loader'
                 ]
                },
                {
                  test: /\.(woff|woff2|eot|ttf|otf)$/,
                  use: ["file-loader"]
                }

               
]
}
};